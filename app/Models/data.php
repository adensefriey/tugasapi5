<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class data extends Model
{
    protected $table = 'biodata';
    protected $fillable = [
        'nama', 'alamat', 'ayah', 'ibu', 'anak-ke'
    ];
}
